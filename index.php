<?php

require('animal.php');
require('frog.php');
require('ape.php');

$object = new kambing("Kambing");

echo "Nama Hewan : $object->name <br>";
echo "Legs : $object->kaki <br>";
echo "Cold Blooded  : $object->darah <br> <br>";

$object2 = new kodok("Frog");
echo "Nama Hewan : $object2->name <br>";
echo "Legs : $object2->roda <br>";
echo "Cold Blooded : $object2->darah <br>";
$object2->jalan();
echo "<br> <br>";

$object3 = new monyet("Monkey");
echo "Nama Hewan : $object3->name <br>";
echo "Legs : $object3->roda2 <br>";
echo "Cold Blooded : $object3->darah <br>";
$object3->jalan2();

?>